<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use AppBundle\Entity\User;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:create-users')
            ->setDescription('Creates new users.')
            ->setHelp("This command allows you to create users...")

            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.')
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('role', InputArgument::REQUIRED, 'The role of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('You are about to ');
        $output->write('create a user.');

        $user = new User();
        $user->setUsername($input->getArgument('username'));
        $password = $this->getContainer()->get('security.password_encoder')
            ->encodePassword($user, $input->getArgument('password'));
        $user->setPassword($password);

        $user->setPassword($password);
        $user->setEmail($input->getArgument('email'));
        $user->setRoles($input->getArgument('role'));

        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $em->persist($user);
        $em->flush();

        $output->writeln('Username: '.$input->getArgument('username'));
    }
}
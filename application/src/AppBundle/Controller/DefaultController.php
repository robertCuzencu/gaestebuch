<?php

namespace AppBundle\Controller;
use AppBundle\Entity\GuestbookEntries;
use AppBundle\Entity\User;
use AppBundle\Entity\Commentary;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\GuestbookForm1;
use AppBundle\Form\CommentaryForm1;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class DefaultController extends Controller
{
    public function useGuestbookForm1Action(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entry = new GuestbookEntries();
        $form = $this->createForm(GuestbookForm1::class, $entry);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entry = $form->getData();
                $entry->setUser($this->getUser());
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($entry);
                    $this->getUser()->setEntries($entry);
                    $em->flush();
                } catch (\Exception $e) {
                    $this->addFlash('error', sprintf('A %s was thrown when trying to persist 
                              the entities with message = %s', get_class($e), $e->getMessage()));
                }
                $this->addFlash('success', 'Successfully saved informations');
                return $this->redirectToRoute('nlist');
            }
            else {
                $this->addFlash('error', 'Not saved informations');
            }
        }
        return $this->render('guestbook\useGuestbookForm1.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function showAllGuestbookEntriesAction($page, $listSize)
    {
        $maxResults = $this->getDoctrine()->getRepository('AppBundle:GuestbookEntries')->getList( true );
        $paginator = $this->get('paginator');
        $paginator->init($page, $maxResults, $listSize);
        $entries = $this->getDoctrine()->getRepository('AppBundle:GuestbookEntries')->getList( false, $paginator->getOffset(), $listSize );

        return $this->render('guestbook\ShowAllGuestbookEntries.html.twig', array('paginator' => $paginator, 'entries' => $entries, 'listSize' => $listSize,
        ));
    }

    public function deleteEntriesAction(GuestbookEntries $entry)
    {
        if( $entry and ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') or ($this->get('security.authorization_checker')->isGranted('ROLE_USER') and $this->getUser()->getId() == $entry->getUser()->getId())) ) {
            $entry->getUser()->removeEntry($entry);
            $em = $this->getDoctrine()->getManager();
            $em->remove($entry);
            $em->flush();
            $this->addFlash('success', 'Successfully deleted informations');
        }
        else {
            $this->addFlash('error', 'Error on delete');
        }
        return $this->redirectToRoute('nlist');
    }

    public function showUserAction(User $user, $page, $listSize)
    {
        if (!($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') or ($this->get('security.authorization_checker')->isGranted('ROLE_USER') and $this->getUser()->getId() == $userId))) {
            $this->addFlash('error', 'You musst have certain rights in order to perform this action.');
            return $this->redirectToRoute('login');
        }

        $maxResults = count($user->getEntries());
        $paginator = $this->get('paginator');
        $paginator->init($page, $maxResults, $listSize);

        return $this->render('guestbook\ShowUser.html.twig', array('paginator' => $paginator, 'user' => $user, 'listSize' => $listSize));
    }

    public function showAllUsersAction($page, $listSize)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $maxResults = $this->getDoctrine()->getRepository('AppBundle:User')->getList( true );
        $paginator = $this->get('paginator');
        $paginator->init($page, $maxResults, $listSize);
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->getList( false, $paginator->getOffset(), $listSize );

        return $this->render('guestbook\ShowAllUsers.html.twig', array('paginator' => $paginator, 'users' => $users, 'listSize' => $listSize));

    }

    public function useCommentaryForm1Action(GuestbookEntries $entry, Request $request)
    {
        if (!($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') or ($this->get('security.authorization_checker')->isGranted('ROLE_USER')))) {
            $this->addFlash('error', 'You musst have certain rights in order to perform this action.');
            return $this->redirectToRoute('login');
        }

        $commentary = new Commentary();
        $form = $this->createForm(CommentaryForm1::class, $commentary);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $commentary = $form->getData();

                $commentary->setUser($this->getUser());
                $commentary->setEntries($entry);

                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($commentary);

                    $this->getUser()->setCommentariesInUser($commentary);
                    $entry->setCommentariesInGuestbookEntries($commentary);

                    $em->flush();
                } catch (\Exception $e) {
                    $this->addFlash('error', sprintf('A %s was thrown when trying to persist
                              the entities with message = %s', get_class($e), $e->getMessage()));
                }
                $this->addFlash('success', 'Successfully saved informations');
                return $this->redirectToRoute('nlist');
            } else {
                $this->addFlash('error', 'Not saved informations');
            }
        }

        return $this->render('guestbook\useCommentaryForm1.html.twig', array('formCommentary'=> $form->createView(), 'entryId' => $entry->getId()
        ));
    }
    
    public function assessAction(GuestbookEntries $entry, $assessValue, $page)
    {
        if (!($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') or ($this->get('security.authorization_checker')->isGranted('ROLE_USER')))) {
            $this->addFlash('error', 'You musst have certain rights in order to perform this action.');
            return $this->redirectToRoute('login');
        }
        /*$entry = $this->getDoctrine()
            ->getRepository('AppBundle:GuestbookEntries')
            ->find($entryId);*/

        if(in_array($this->getUser()->getId(), $entry->getUserAlreadyVoted()) ) {
            $this->addFlash('error', 'You already voted.');
            return $this->redirectToRoute('nlist');
        }
        if($this->getUser() === $entry->getUser()->getId() ) {
            $this->addFlash('error', 'You are not allowed to vote your own entry. Angeber!!');
            return $this->redirectToRoute('nlist');
        }

        $entry->setUserAlreadyVoted($this->getUser()->getId());

        $newAssessValue = $entry->getAssessValue()+$assessValue;
        $newAssessNumber = $entry->getAssessNumber()+1;
        $entry->setAssessValue($newAssessValue);
        $entry->setAssessNumber($newAssessNumber);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entry);
        $em->flush();

        $this->addFlash('success', 'Thank you. Successfully added Assessment.');
        return $this->redirectToRoute('nlist', array('page' => $page));
    }

    public function testAction() {
        return new Response("Das ist ein Test");
    }
}
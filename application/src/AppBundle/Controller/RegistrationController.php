<?php

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\UserTypeForm;

class RegistrationController extends Controller
{
    public function registrationAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserTypeForm::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $password = $this->get('security.password_encoder')
                    ->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $user->setRoles('ROLE_USER');

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Successfully registered!');
                return $this->redirectToRoute('nlist');
            }
            else {
                $this->addFlash('error', 'Not registered. Invalid data entry.');
                $this->redirectToRoute('registration');
            }
        }

        return $this->render(
            'Registration/Registration.html.twig',
            array('form' => $form->createView())
        );
    }
}
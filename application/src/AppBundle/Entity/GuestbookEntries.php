<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GuestbookRepository")
 * @ORM\Table(name="GuestbookEntries")
 * @ORM\HasLifecycleCallbacks()
 */
class GuestbookEntries
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="entries")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=FALSE)
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */

    private $entry;
    /**
     * @ORM\Column(type="date")
     *
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="Commentary", mappedBy="entries")
     */
    private $commentariesInGuestbookEntries;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $assessValue = 0;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $assessNumber = 0;

    /**
     * Get Events
     *
     * @return mixed
     */

    /**
     * @ORM\Column(name="userAlreadyVoted", type="array", nullable=true)
     *
     */
    private $userAlreadyVoted = array();

    public function __construct()
    {
        $this->commentariesInGuestbookEntries = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * @param mixed $entry
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getCommentariesInGuestbookEntries()
    {
        return $this->commentariesInGuestbookEntries;
    }

    /**
     * @param mixed $commentariesInGuestbookEntries
     */
    public function setCommentariesInGuestbookEntries($commentariesInGuestbookEntries)
    {
        if (!$this->commentariesInGuestbookEntries->contains($commentariesInGuestbookEntries)) {
            $this-> commentariesInGuestbookEntries->add($commentariesInGuestbookEntries);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssessValue()
    {
        return $this->assessValue;
    }

    /**
     * @param mixed $assessValue
     */
    public function setAssessValue($assessValue)
    {
        $this->assessValue = $assessValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssessNumber()
    {
        return $this->assessNumber;
    }

    /**
     * @param mixed $assessNumber
     */
    public function setAssessNumber($assessNumber)
    {
        $this->assessNumber = $assessNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserAlreadyVoted()
    {
        return $this->userAlreadyVoted;
    }

    /**
     * @param mixed $userAlreadyVoted
     */
    public function setUserAlreadyVoted($userAlreadyVoted)
    {
        if (!in_array($userAlreadyVoted, $this->userAlreadyVoted)) {
            array_push($this->userAlreadyVoted, $userAlreadyVoted);
        }
        return $this;
    }
}

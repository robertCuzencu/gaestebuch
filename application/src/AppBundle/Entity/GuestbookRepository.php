<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GuestbookRepository extends EntityRepository {

    public function findAllOrderedByDate()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT g FROM AppBundle:GuestbookEntries g ORDER BY g.createdAt DESC'
            )
            ->getResult();
    }

    public function getList( $count = false, $offset = 0, $limit = 50)
    {
        $query = $this->createQueryBuilder('g')
            ->orderBy('g.createdAt', 'DESC');

        if ($count === false) {
            return $query
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->execute();
        }
        $query->select('count(g.id)');
        return $query->getQuery()->getSingleScalarResult();
    }

    public function findAllUsersOrderedByName()
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:User')->createQueryBuilder('u')
            ->orderBy('u.username', 'ASC')
            ->getQuery();
        $users = $query->getResult();
    }

}
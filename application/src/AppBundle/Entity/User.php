<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
/**
* @ORM\Table(name="app_users")
* @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
*/
class User implements UserInterface, \Serializable
{
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
    * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\NotBlank()
    */
    private $username;

    /**
    * @ORM\Column(type="string", length=64)
    */
    private $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
    * @ORM\Column(type="string", length=60, unique=true)
    */
    private $email;

    /**
    * @ORM\Column(name="is_active", type="boolean")
    */
    private $isActive;

    /**
     * @ORM\Column(name="roles", type="string")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="GuestbookEntries", mappedBy="user")
     */
    private $entries;

    /**
     * @ORM\OneToMany(targetEntity="Commentary", mappedBy="user")
     */
    private $commentariesInUser;


    /**
     * Get Events
     *
     * @return mixed
     */

    public function __construct()
    {
        $this->isActive = true;
        $this->entries = new ArrayCollection();
        $this->commentariesInUser = new ArrayCollection();
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        return $this;
    }

    public function getRoles()
    {   $roles = $this->roles;
        return array($roles);
    }

    public function setRoles($roles) {
        $roles = strtoupper($roles);
        $this->roles = $roles;

        return $this;
    }
    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
        $this->id,
        $this->username,
        $this->password,
        // see section on salt below
        // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function getEntries()
    {
        return $this->entries->toArray();
    }


    public function setEntries($entry)
    {
        if (!$this->entries->contains($entry)) {
            $this->entries->add($entry);
        }
        return $this;
    }

    public function removeEntries($entry)
    {
        if ($this->entries->contains($entry)) {
            $this->entries->removeElement($entry);
        }
    }

    /**
     * @return mixed
     */
    public function getCommentariesInUser()
    {
        return $this->commentariesInUser;
    }

    /**
     * @param mixed $commentariesInUser
     */
    public function setCommentariesInUser($commentariesInUser)
    {
        if (!$this->commentariesInUser->contains($commentariesInUser)) {
            $this->commentariesInUser->add($commentariesInUser);
        }
    }
}

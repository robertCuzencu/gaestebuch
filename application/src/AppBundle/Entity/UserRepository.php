<?php

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
        ->where('u.username = :username OR u.email = :email')
        ->setParameter('username', $username)
        ->setParameter('email', $username)
        ->getQuery()
        ->getOneOrNullResult();
    }
    public function getList( $count = false, $offset = 0, $limit = 50)
    {
        $query = $this->createQueryBuilder('g')
        ->orderBy('g.username', 'ASC');

        if ($count === false) {
            return $query
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->execute();
        }
        $query->select('count(g.id)');
        return $query->getQuery()->getSingleScalarResult();
    }

    public function findAllUsersOrderedByName()
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:User')->createQueryBuilder('u')
            ->orderBy('u.username', 'ASC')
            ->getQuery();
        $users = $query->getResult();
    }
}
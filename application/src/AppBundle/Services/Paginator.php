<?php
/**
 * Created by PhpStorm.
 * User: Robert Cuzencu
 * Date: 23.09.2016
 * Time: 15:35
 */

namespace AppBundle\Services;
/**
 * Class Paginator
 */
final class Paginator
{
    /**
     * @var int size of the actually list
     */
    public $count;
    /**
     * @var int active page
     */
    public $page;
    /**
     * @var int count of pages ceiled by listsize
     */
    public $pages;
    /**
     * @var int entries per page
     */
    public $listsize;
    /**
     * @var int offset for the list
     */
    private $offset;
    /**
     * @var array this array has the complete sitenumbers with active/inactive info
     */
    private $list = [];
    /**
     * Generate paging
     *
     * @param int $page
     * @param int $maxCount
     * @param int $listsize
     *
     * @return mixed $this
     */
    public function init($page, $maxCount, $listsize)
    {
        $pages = ceil($maxCount/$listsize);
        if (1 > $pages) {
            $pages = 1;
        }
        for ($i = 1; $i < ($pages+1); $i++) {
            if ($i == $page) {
                $active = true;
            } else {
                $active = false;
            }
            $this->list[] = [
                'number'    => $i,
                'active'    => $active
            ];
        }
        if (0 > (($page-1)*$listsize)) {
            $this->offset = 0;
        } else {
            $this->offset = (($page-1)*$listsize);
        }
        $this->page     = $page;
        $this->pages    = $pages;
        $this->listsize = $listsize;
        $this->count    = $maxCount;
        return $this;
    }
    /**
     * Get list
     *
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }
    /**
     * Get offset
     *
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }
    /**
     * Get list count
     *
     * @return mixed
     */
    public function getCount()
    {
        return count($this->list);
    }
    /**
     * Get max results count
     *
     * @return int
     */
    public function getMaxResultsCount()
    {
        return $this->count;
    }
}